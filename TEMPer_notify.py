#! /usr/bin/env python2
# -*- coding: utf-8 -*-

import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText

class TEMPer_notify():
    def __init__(self, TO):
        self.mailing_state = False
        self.TO = TO
        self.FROM = "tempernotify@gmail.com"
        self.password = ""
        self.mail_server = smtplib.SMTP('smtp.gmail.com', 587)

    def send(self, subject, content):
        message = MIMEMultipart()
        message['From'] = self.FROM
        message['To'] = ", ".join(self.TO)
        message['Subject'] = subject
        message.attach(MIMEText(content.encode('utf-8'), 'plain', 'UTF-8'))

        try:
            # Identify yourself to an ESMTP server
            self.mail_server.ehlo()
            self.mail_server.starttls()
            self.mail_server.ehlo()
            self.mail_server.login(self.FROM, self.password)
            self.mail_server.sendmail(self.FROM, self.TO,
                                      message.as_string())
        except smtplib.SMTPHeloError:
            print "The server didn't reply properly to the HELO greeting."
        except smtplib.SMTPException:
            print "The server does not support the STARTTLS extension."
        except smtplib.SMTPAuthenticationError:
            print "The server did not accept the username/pass combination."
        except RuntimeError:
            print "SSL/TLS is not supported by your Python interpreter."
        except smtplib.SMTPRecipientsRefused:
            print "All recipients, ", self.TO, ", were refused."
        except smtplib.SMTPSenderRefused:
            print "The server did not accept ", self.FROM, "."
        except smtplib.SMTPDataError:
            print "The server replied with an unexpected error code"
        else:
            self.mailing_state = True

        self.mail_server.close()

