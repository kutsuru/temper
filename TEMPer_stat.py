#! /usr/bin/env python2
# -*- coding: utf-8 -*-

import os

import matplotlib
matplotlib.use('GTKAgg')

from datetime import datetime
from matplotlib.dates import date2num

import matplotlib.pyplot as plt
import gobject

import csv

from TEMPer_notify import TEMPer_notify

from optparse import OptionParser

class TEMPer():
    def __init__(self, options):

        # Set options value
        self.log_mode = options.log
        self.interactive_mode = options.interactive

        self.interval = options.interval
        self.temperature_limit = options.temperature_limit

        if (options.log):

            # if the subdirectory log does not exist create it
            if (not os.path.isdir('log/')):
                try:
                    os.mkdir('log')
                except OSError:
                    raise

            self.decount = 0
            self.log_interval = options.log_interval * 60

        if (options.recipients is not None):
            self.recipients = options.recipients
            self.notify = TEMPer_notify(self.recipients)

        # Init plot axis
        self.timestamp = plt.np.array([date2num(datetime.now())])
        self.sensor_in = plt.np.array([0])
        self.sensor_out = plt.np.array([0])

        self.TEMPer_init()

        # Handle the main loop through a gobject
        gobject.timeout_add_seconds(self.interval, self.TEMPer_update)

        # Handle key event
        self.figure.canvas.mpl_connect('key_press_event',
                                       self.TEMPer_handle_key)

        # Display the plot only in interative mode
        plt.show()

    def compute_TEMPer(self, TEMPer_value):
        """
        Transform the raw value provides by the sensor to a celsius value.
        """

        return (round(TEMPer_value * 125. / 32000., 2))

    def find_TEMPer_path(self, path, filename):
        """
        Retrieve the files which contain the sensor value.
        """

        for root, _, names in os.walk(path, followlinks=True):
            if filename in names:
                return os.path.join(root, filename)

    def get_TEMPer_values(self):
        """
        Read the sensor value from two predefined files in the sysfs.
        """

        TEMPer_in_file = open(self.TEMPer_in_path, 'r+')
        TEMPer_out_file = open(self.TEMPer_out_path, 'r+')

        TEMPer_in_value = self.compute_TEMPer(int(TEMPer_in_file.read()))
        TEMPer_out_value = self.compute_TEMPer(int(TEMPer_out_file.read()))

        TEMPer_in_file.close()
        TEMPer_out_file.close()

        return TEMPer_in_value, TEMPer_out_value

    def TEMPer_init(self):
        """
        Initialization of the plotting.
        """

        self.figure = plt.figure()

        plt.xlabel('Timestamp')
        plt.ylabel('Temperature')

        self.axis = self.figure.add_subplot(111)
        self.canvas = self.axis.figure.canvas
        self.background = self.canvas.copy_from_bbox(self.axis.bbox)

        self.line_in, = self.axis.plot_date(self.timestamp, self.sensor_in,
                                            lw = 1, animated = True,
                                            color = "orange",
                                            label = "In Sensor")

        self.line_out, = self.axis.plot_date(self.timestamp, self.sensor_out,
                                             lw = 1, animated = True,
                                             color = "grey",
                                             label = "Out Sensor")
        self.axis.set_ylim(20, 40)
        self.axis.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=1, ncol=6,
                         mode="expand", borderaxespad=0.)

        path = '/sys/bus/usb/drivers/TEMPer/'
        self.TEMPer_in_path = self.find_TEMPer_path(path, 'TemperatureIn')
        self.TEMPer_out_path = self.find_TEMPer_path(path, 'TemperatureOut')

    def TEMPer_handle_key(self, event):

        if event.key == 'q':
            plt.close()
        if event.key == '+':
            current_ylim = self.axis.get_ylim()
            self.axis.set_ylim(current_ylim[0] * 0.5, current_ylim[1] * 1.5)
        if event.key == '-':
            current_ylim = self.axis.get_ylim()
            self.axis.set_ylim(current_ylim[0] * 1.5, current_ylim[1] * 0.5)
        else:
            pass

    def TEMPer_update(self):
        """
        Update callback that will be used along the timer in the main loop.
        """

        status = True
        current_time = date2num(datetime.now())

        temperature_in, temperature_out = self.get_TEMPer_values()

        if (self.interactive_mode):
            status = self.TEMPer_update_plot(temperature_in, temperature_out,
                                             current_time)

        if (self.log_mode):
            self.decount -= self.interval

            status = self.TEMPer_log(temperature_in, temperature_out)

        return status

    def TEMPer_notify(self):

        if ((temperature_in > self.temperature_limit
            or temperature_out > self.temperature_limit)
            and not self.notify.mailing_state):

            subject = "[TEMPer] Over 9k Celsius"
            content = u"""
            This is a TEMP(t)est.
            The inner sensor has now reached\t --> %10.3f \u2103.
            And the outter sensor\t\t\t--> %10.3f \u2103
            """ % (temperature_in, temperature_out)

            self.notify.send(subject, content)

    def TEMPer_log(self, temperature_in, temperature_out):

        current_time = str(datetime.now())

        # Deduct the filename from the current time
        if (self.decount <= 0):

            if hasattr(self, 'fd_log_file'):
                self.fd_log_file.close()

            self.log_file = "log/" + current_time
            self.decount = self.log_interval

            self.fd_log_file = open(self.log_file, 'ab')
            self.log_writer = csv.writer(self.fd_log_file, delimiter = '\t',
                                         quotechar = '|',
                                         quoting = csv.QUOTE_ALL)

        self.log_writer.writerow([current_time, temperature_in,
                                  temperature_out])

        return True

    def TEMPer_update_plot(self, temperature_in, temperature_out, time):

        self.timestamp = plt.np.append(self.timestamp, time)

        self.sensor_in = plt.np.append(self.sensor_in, temperature_in)
        self.sensor_out = plt.np.append(self.sensor_out, temperature_out)

        self.canvas.restore_region(self.background)

        # Update axis
        self.axis.set_xlim(self.timestamp[0],
                           self.timestamp[plt.np.size(self.timestamp) - 1])

        self.line_in.set_data(self.timestamp, self.sensor_in)
        self.line_out.set_data(self.timestamp, self.sensor_out)

        self.canvas.draw()

        try:
            self.axis.draw_artist(self.line_in)
            self.axis.draw_artist(self.line_out)
        except AssertionError:
            return False

        try:
            self.canvas.blit(self.axis.bbox)
        except AttributeError:
            return False

        return True

    def __del__(self):

        if (options.log and hasattr(self, 'fd_log_file')
            and self.fd_log_file.isopen()):
            self.fd_log_file.close()

# Options handling

def main():
    option_parser = OptionParser()

    option_parser.add_option("-i", "--interval",
                             dest = "interval",
                             type = "int",
                             default = 1,
                             help = "set the interval between each plot, in\
                             seconds")

    option_parser.add_option("-I", "--interactive",
                             action = "store_true",
                             dest = "interactive",
                             default = False,
                             help = "display a real time plotting")

    option_parser.add_option("-l", "--log",
                             action = "store_true",
                             dest = "log",
                             default = False,
                             help = "activate the log of the temperature")

    option_parser.add_option("-L", "--log-interval",
                             dest = "log_interval",
                             type = "int",
                             default = 60,
                             help = "set the interval to log into another file,\
                             in minutes")

    option_parser.add_option("-n", "--notify",
                             action = "append",
                             dest = "recipients",
                             default = None,
                             help = "notify a specified recipients of abnormal\
                             temperature")

    option_parser.add_option("-t", "--temperature-limit",
                             dest = "temperature_limit",
                             type = "int",
                             default = 30,
                             help = "set the temperature treshold notification")


    (options, args) = option_parser.parse_args()

    temper = TEMPer(options)

if __name__ == "__main__":
    main()
