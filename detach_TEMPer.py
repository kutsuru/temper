#! /usr/bin/env python2
# -*- coding: utf-8 -*-

import usb.core

def detach_TEMPer():
    """
    Detach the TEMPer device from the usbhid.
    """

    device = usb.core.find(idVendor=0x0c45, idProduct=0x7401)

    if device != None:
        # Retrieve the current configuration to find out the number of
        # interfaces
        configuration = device.get_active_configuration()

        for i in xrange (configuration.bNumInterfaces):
            if device.is_kernel_driver_active(i):
                device.detach_kernel_driver(i)

        print "TEMPer successfully detached from kernel"

if __name__ == "__main__":
    detach_TEMPer()
