/**
** Linux USB driver for the TEMPer thermal sensor contructed by Microdia.
** This version is an improvement of Pierre Ficheux's one originally designed
** for the TEMPer v1.0.
**/ 

#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/usb.h>

#define DRIVER_AUTHOR "Pierre Ficheux, pierre.ficheux@gmail.com\n\
    \t\tLoïc Castelin, kutsuru@gmail.com"
#define DRIVER_DESC   "TEMPer driver v2.23 (Microdia)"

/**
** Device information
**/
#define VENDOR_ID	0x0c45
#define PRODUCT_ID	0x7401

/**
** HZ is an architecture dependant value defined in the linux headers.
**/
#define TIMEOUT         2 * HZ

/**
** Temperature request byte stream.
**/
static char uTemperature[] = { 0x01, 0x80, 0x33, 0x01, 0x00, 0x00,
                               0x00, 0x00 };

/**
** Wrapper of the usb_device structure to store the temperature values.
**/
struct usb_temper {
    struct usb_device *udev;
    int temperatureIn;
    int temperatureOut;
};

/**
** Table of devices that work with this driver
**/
static struct usb_device_id id_table [] = {
    { USB_DEVICE(VENDOR_ID, PRODUCT_ID) },
    { },
};

/**
** This must be set or the driver's probe function will never get called.
**/
MODULE_DEVICE_TABLE (usb, id_table);

/**
** Retrieves the temperature coming from the two thermal sensor.
** @param temper_dev
**     Pointer to the TEMPer usb device to send the message to.
** @param pipe
**     Pipe that you want to use
** @param temperature
**     Where you want to store the temperature value
**/
static int get_temperature_value(struct usb_temper  *temper_dev)
{
    int ret = 0;
    int length = 0;

    char buf[8];

    // Retrieve the temperature value
    // Initialize the buffer with the temperature request data

    memcpy(buf, uTemperature, 8);
    ret = usb_control_msg(temper_dev->udev,
                          usb_sndctrlpipe(temper_dev->udev, 0),
                          0x09, 0x21, 0x200, 0x01, buf, 8, TIMEOUT);

    if (ret < 0)
        printk(KERN_WARNING "%s: usb_control_msg(), ret = %d\n",
               __FUNCTION__, ret);

    // Reset the buffer to receive the result of the previous request
    memset(buf, 0, sizeof (buf));
    ret = usb_interrupt_msg(temper_dev->udev,
                            usb_rcvintpipe (temper_dev->udev, 2), &buf, 8,
                            &length, TIMEOUT);
    if (ret < 0) 
        printk(KERN_WARNING "%s: usb_interrupt_msg(), ret = %d length = %d\n",
               __FUNCTION__, ret, length);
    else
    {
        temper_dev->temperatureOut = (buf[5] & 0xff) + (buf[4] << 8);
        temper_dev->temperatureIn  = (buf[3] & 0xff) + (buf[2] << 8);
    }

    return ret;
}

static ssize_t show_temperature_in(struct device              *dev,
                                   struct device_attribute    *attr,
                                   char                       *buf)
{
    struct usb_interface *intf = to_usb_interface(dev); 
    struct usb_temper *temper_dev = usb_get_intfdata(intf);

    get_temperature_value(temper_dev);

    return sprintf(buf, "%d\n", temper_dev->temperatureIn);
}

static ssize_t show_temperature_out(struct device              *dev,
                                    struct device_attribute    *attr,
                                    char                       *buf)
{
    struct usb_interface *intf = to_usb_interface(dev); 
    struct usb_temper *temper_dev = usb_get_intfdata(intf);

    get_temperature_value(temper_dev);

    return sprintf(buf, "%d\n", temper_dev->temperatureOut);
}

static ssize_t set_temp(struct device           *dev,
                        struct device_attribute *attr,
                        const char              *buf,
                        size_t                   count)
{
    return count;
}

/**
** Create a file in the sysfs for the first thermal sensor.
** @param TemperatureIn
**     Name of the file in the sysfs
** @param S_IWUGO
**     (S_IWUSR|S_IWGRP|S_IWOTH) -> Access Permission 222 (write)
** @param S_IRUGO
**     (S_IRUSR|S_IRGRP|S_IROTH) -> Access Permission 444 (read)
** @param show_temperature_in
**     Function to get the temperature from the first thermal sensor
** @param set_temp
**     Dummy function, not use in this case
**/
static DEVICE_ATTR(TemperatureIn, S_IWUGO | S_IRUGO, show_temperature_in,
                   set_temp);

/**
** Create a file in the sysfs for the first thermal sensor.
** @param TemperatureIn
**     Name of the file in the sysfs
** @param S_IWUGO
**     (S_IWUSR|S_IWGRP|S_IWOTH) -> Access Permission 222 (write)
** @param S_IRUGO
**     (S_IRUSR|S_IRGRP|S_IROTH) -> Access Permission 444 (read)
** @param show_temperature_out
**     Function to get the temperature from the second thermal sensor
** @param set_temp
**     Dummy function, not use in this case
**/

static DEVICE_ATTR(TemperatureOut, S_IWUGO | S_IRUGO, show_temperature_out,
                   set_temp);

static int temper_probe(struct usb_interface        *interface,
                        const struct usb_device_id  *id)
{
    struct usb_device *udev = interface_to_usbdev (interface);
    struct usb_temper *temper_dev;
    int ret = 0;

    temper_dev = kmalloc(sizeof (struct usb_temper), GFP_KERNEL);
    if (temper_dev == NULL)
    {
        dev_err (&interface->dev, "Out of memory\n");
        return -ENOMEM;
    }

    // Fill private structure and save it
    memset (temper_dev, 0x00, sizeof (*temper_dev));
    temper_dev->udev = usb_get_dev(udev);
    temper_dev->temperatureIn = 0;
    temper_dev->temperatureOut = 0;
    usb_set_intfdata(interface, temper_dev);

    ret = device_create_file(&interface->dev, &dev_attr_TemperatureIn);
    ret = device_create_file(&interface->dev, &dev_attr_TemperatureOut);

    if (ret < 0)
    {
        printk(KERN_WARNING "%s: device_create_file() error= %d\n",
               __FUNCTION__, ret);
        return ret;
    }

    dev_info(&interface->dev, "USB TEMPer device now attached\n");

    return 0;
}

static void temper_disconnect(struct usb_interface *interface)
{
    struct usb_temper *dev;

    dev = usb_get_intfdata(interface);
    usb_set_intfdata(interface, NULL);

    device_remove_file(&interface->dev, &dev_attr_TemperatureIn);
    device_remove_file(&interface->dev, &dev_attr_TemperatureOut);
    usb_put_dev(dev->udev);

    kfree(dev);

    dev_info(&interface->dev, "USB TEMPer now disconnected\n");
}

/** 
** Identifies the USB interface driver to usbcore.
** @param name
**     The driver name should be unique among USB drivers, and should be the
**     same as the module name.
** @param probe
**     Called to see if the driver is willing to manage a particular interface
**     on a device.
** @param disconnect
**     Called when the interface is no longer accessible, usually when the
**     device has been unplugued or the module unloaded.
** @parama id_table
**     USB drivers use ID table to support hotplugging
**/
static struct usb_driver temper_driver = {
    .name       =	"TEMPer",
    .probe      =	temper_probe,
    .disconnect =	temper_disconnect,
    .id_table   =	id_table,
};

static int __init usb_temper_init(void)
{
    int ret = 0;

    // Registers a USB interface driver with the usb core.
    ret = usb_register(&temper_driver);
    if (ret)
        err("usb_register failed. Error number %d", ret);

    return ret;
}

static void __exit usb_temper_exit(void)
{
    // Unlinks the specified driver from the internal USB driver list.
    usb_deregister(&temper_driver);
}

module_init(usb_temper_init);
module_exit(usb_temper_exit);

MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
MODULE_LICENSE("GPL");
