                               .
    _______   ____ _____     __| _/_____   ____
    \_  __ \_/ __ \\__  \   / __ |/     \_/ __ \
     |  | \/\  ___/ / __ \_/ /_/ |  Y Y  \  ___/
     |__|    \___  >____  /\____ |__|_|  /\___  >
                 \/     \/      \/     \/     \/

-   First, you have to detach the device from the usbhid:

        # run
        ./detach_TEMPer.py

        # or copy:
        cp 10-TEMPer.rules /lib/udev/rules.d/

-   Then create the module based on your system:

        run "make" in the current directory

-   You will have to load the module to the kernel for that you will need the
    root privelege:

        run "su -c "insmod TEMPer.ko""

-   Now you can admire (or not) the value with a realtime
    plot:

        run "./TEMPer_stat.py [options]"
